<?php

use \Psr\Http\Message\ServerRequestInterface as Request;
use \Psr\Http\Message\ResponseInterface as Response;

$app->get('/', function (Request $request, Response $response, $args) {

    try {
        $stmt = $this->db->prepare("SELECT id_person,nickname,first_name,last_name,birth_day,height,gender,id_location FROM person");
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }

    $tplVars['persons'] = $stmt->fetchAll();

    return $this->view->render($response, 'index.latte', $tplVars);
})->setName('index');

$app->get('/new-person', function (Request $request, Response $response, $args) {


    return $this->view->render($response, 'new-person.latte');
})->setName('newPerson');

$app->post('/new-person', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (empty($data['fn']) || empty($data['ln']) || empty($data['ln'])){
        $tplVars['alert'] = "Chybí jméno, příjmení a přezdívka";
        return $this->view->render($response, 'new-person.latte', $tplVars);

    }
    try {
        $stmt = $this->db->prepare("INSERT INTO person
          (first_name,last_name,nickname,height,birth_day,gender)
          VALUES
          (:fn,:ln,:nn,:h,:bd,:g)");

        $stmt->bindValue(':fn', $data['fn']);
        $stmt->bindValue(':ln', $data['ln']);
        $stmt->bindValue(':nn', $data['nn']);

        if (empty($data['h'])){
            $stmt->bindValue(':h', null);
        } else {
            $stmt->bindValue(':h', $data['h']);
        }

        if (empty($data['bd'])){
            $stmt->bindValue(':bd', null);
        } else {
            $stmt->bindValue(':bd', $data['bd']);
        }

        $stmt->bindValue(':g', $data['g']);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }

    return $response->withHeader('Location', $this->router->pathFor("index"));
});


$app->post('/test', function (Request $request, Response $response, $args) {
    //read POST data
    $input = $request->getParsedBody();

    //log
    $this->logger->info('Your name: ' . $input['person']);

    return $response->withHeader('Location', $this->router->pathFor('index'));
})->setName('redir');

$app->get('/delete-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");
    try {
        $stmt = $this->db->prepare("DELETE FROM person WHERE id_person=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }

    return $response->withHeader('Location', $this->router->pathFor('index'));


})->setName("deletePerson");

$app->get('/edit-person', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");

    try {
        $stmt = $this->db->prepare("SELECT id_person,birth_day,gender,first_name,last_name,nickname,height FROM person WHERE id_person=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }

    $person = $stmt->fetch();
    if (empty($person)) {
        die("osoba nenalezena");
    }

    $tplVars['id'] = $id;
    $tplVars['form'] = [
        'fn' => $person['first_name'],
        'ln' => $person['last_name'],
        'nn' => $person['nickname'],
        'h' => $person['height'],
        'g' => $person['gender'],
        'bd' => $person['birth_day']
    ];

    return $this->view->render($response, 'edit-person.latte', $tplVars);


})->setName("editPerson");

$app->post('/edit-person', function (Request $request, Response $response, $args) {

    $id = $request->getQueryParam("id");
    $data = $request->getParsedBody();

    if (empty($data['fn']) || empty($data['ln']) || empty($data['ln'])) {
        $tplVars['alert'] = "Chybí jméno, příjmení a přezdívka";
        return $this->view->render($response, 'edit-person.latte', $tplVars);
    }

    try {
        $stmt = $this->db->prepare("UPDATE person SET
          first_name = :fn,last_name = :ln, nickname = :nn, height = :h, birth_day = :bd, gender = :g WHERE id_person = :id_person");

        $stmt->bindValue(':id_person', $id);
        $stmt->bindValue(':fn', $data['fn']);
        $stmt->bindValue(':ln', $data['ln']);
        $stmt->bindValue(':nn', $data['nn']);

        if (empty($data['h'])){
            $stmt->bindValue(':h', null);
        } else {
            $stmt->bindValue(':h', $data['h']);
        }

        if (empty($data['bd'])){
            $stmt->bindValue(':bd', null);
        } else {
            $stmt->bindValue(':bd', $data['bd']);
        }

        $stmt->bindValue(':g', $data['g']);
        $stmt->execute();

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }
    return $response->withHeader('Location', $this->router->pathFor('index'));


});

$app->get('/location', function (Request $request, Response $response){
    $id = $request->getQueryParam("id");
    $pid = $request->getQueryParam("pid");
    if (empty($id)){
        $tplVars['alert'] = "chybí adresa";
    } else {

    try {
        $location = $this->db->prepare("SELECT * FROM location WHERE id_location = :id_location");
        $location->bindValue(':id_location', $id);
        $location->execute();
        $tplVars['location'] = $location->fetch();


    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);

        }
    }
    if (isset($pid)){
        $locations = $this->db->query("SELECT * FROM location");
        $tplVars['locations'] = $locations->fetchAll();
    }


    $tplVars['pid'] = $pid;
    return $this->view->render($response, 'location.latte', $tplVars);
})->setName('location');

$app->post('/location', function (Request $request, Response $response){
   $form = $request->getParsedBody();

   try {
       $location = $this->db->prepare("UPDATE person SET id_location = :id_location WHERE id_person = :id_person");
       $location->bindValue(':id_location', $form['id_location']);
       $location->bindValue(':id_person', $form['id_person']);
       $location->execute();

   } catch (PDOException $e) {
       $this->logger->error($e->getMessage());
       die($e);
   }

    return $response->withHeader('Location', $this->router->pathFor('index'));
});

$app->get('/new-location', function (Request $request, Response $response){

    return $this->view->render($response, 'new-location.latte');

})->setName('newLocation');


$app->post('/new-location', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (empty($data['street_name']) || empty($data['street_number']) || empty($data['city']) || empty($data['country'])){
        $tplVars['alert'] = "Missing or wrong values";
        return $this->view->render($response, 'new-location.latte', $tplVars);
    }
    try {
        $stmt = $this->db->prepare("INSERT INTO location
          (name ,street_name,street_number,zip,city,country)
          VALUES
          (:name,:street_name,:street_number,:zip,:city,:country)");
        $stmt->bindValue(':name', $data['name']);
        $stmt->bindValue(':street_name', $data['street_name']);
        $stmt->bindValue(':street_number', $data['street_number']);
        $stmt->bindValue(':zip', $data['zip']);
        $stmt->bindValue(':city', $data['city']);
        $stmt->bindValue(':country', $data['country']);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }

    return $response->withHeader('Location', $this->router->pathFor("index"));
});

$app->get('/new-meeting', function (Request $request, Response $response){
    $meeting = $this->db->query("SELECT * FROM location");
    $tplVars['locations'] = $meeting->fetchAll();
    return $this->view->render($response, 'new-meeting.latte', $tplVars);

})->setName('newMeeting');

$app->post('/new-meeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (empty($data['start']) || empty($data['description']) || empty($data['duration'])){
       $tplVars['alert'] = "Missing or wrong values";
        return $this->view->render($response, 'new-meeting.latte', $tplVars);
    }
    try {
        $stmt = $this->db->prepare("INSERT INTO meeting
          (id_meeting, start,description,duration,id_location)
          VALUES
          (nextval('meeting_id_meeting_seq'), :start,:description,:duration,:id_location)");
        $stmt->bindValue(':start', $data['start']);
        $stmt->bindValue(':description', $data['description']);
        $stmt->bindValue(':duration', $data['duration']);
        $stmt->bindValue(':id_location', $data['id_location']);
        $stmt->execute();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());
    }

    return $response->withHeader('Location', $this->router->pathFor("meetings"));
});

$app->get('/meeting', function (Request $request, Response $response){
    $id = $request->getQueryParam("id");
    try {
        $meetings = $this->db->prepare("SELECT * FROM meeting JOIN person_meeting ON meeting.id_meeting = person_meeting.id_meeting WHERE person_meeting.id_person = :id_person");

        $meetings->bindValue(':id_person', $id);
        $meetings->execute();

        $tplVars['meetings'] = $meetings->fetchAll();

        $all = $this->db->query("SELECT * FROM meeting");
        $tplVars['all'] = $all->fetchAll();
        $tplVars['pid'] = $id;
    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);

    }

    return $this->view->render($response, 'meeting.latte', $tplVars);

})->setName('meeting');

$app->post('/meeting', function (Request $request, Response $response){
    $form = $request->getParsedBody();

    try {
        $location = $this->db->prepare("INSERT INTO person_meeting (id_person, id_meeting) VALUES (:id_person, :id_meeting)");

        $location->bindValue(':id_person', $form['id_person']);
        $location->bindValue(':id_meeting', $form['id_meeting']);
        $location->execute();
    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);
    }

    return $response->withHeader('Location', $this->router->pathFor('index'));
});

$app->get('/meetings', function (Request $request, Response $response){
    try {
        $meetings = $this->db->query("SELECT * FROM meeting ORDER BY start");

        $tplVars['meetings'] = $meetings->fetchAll();

    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);

    }

    return $this->view->render($response, 'meetings.latte', $tplVars);

})->setName('meetings');


$app->get('/delete-meeting', function (Request $request, Response $response){
    $id = $request->getQueryParam("id");

    try {
        $delete = $this->db->prepare("DELETE FROM meeting WHERE id_meeting = :id_meeting");
        $delete->bindValue(":id_meeting", $id);
        $delete->execute();

    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);

    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));

})->setName('deleteMeeting');

$app->get('/edit-meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");

    try {
        $stmt = $this->db->prepare("SELECT * FROM meeting WHERE id_meeting=:id");
        $stmt->bindValue(":id", $id);
        $stmt->execute();
        $tplVars['meeting'] = $stmt->fetch();
        $locations = $this->db->query("SELECT * FROM location");
        $tplVars['locations'] = $locations->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }

    return $this->view->render($response, 'edit-meeting.latte', $tplVars);


})->setName("editMeeting");

$app->post('/edit-meeting', function (Request $request, Response $response, $args) {
    $data = $request->getParsedBody();
    if (empty($data['start']) || empty($data['description']) || empty($data['duration'])){
        $tplVars['alert'] = "Missing or wrong values";
        return $this->view->render($response, 'new-meeting.latte', $tplVars);
    }
    try {
        $stmt = $this->db->prepare("UPDATE meeting SET start=:start, description=:description, duration=:duration, id_location=:id_location WHERE id_meeting=:id_meeting");

        $stmt->bindValue(':start', $data['start']);
        $stmt->bindValue(':description', $data['description']);
        $stmt->bindValue(':duration', $data['duration']);
        $stmt->bindValue(':id_location', $data['id_location']);
        $stmt->bindValue(':id_meeting', $data['id_meeting']);
        $stmt->execute();

    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);

    }
    return $response->withHeader('Location', $this->router->pathFor('meetings'));


});

$app->get('/leave-meeting', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");
    $m = $request->getQueryParam("m");

    try {
        $stmt = $this->db->prepare("DELETE FROM person_meeting WHERE id_person = :id_person AND id_meeting = :id_meeting");
        $stmt->bindValue(":id_person", $id);
        $stmt->bindValue(":id_meeting", $m);
        $stmt->execute();

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }
    return $response->withHeader('Location', $this->router->pathFor('index'));

})->setName('leaveMeeting');


$app->get('/contacts', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");

    try {
        $stmt = $this->db->prepare("SELECT contact_type.name, contact, id_contact FROM contact JOIN contact_type ON contact.id_contact_type=contact_type.id_contact_type WHERE contact.id_person = :id_person");
        $stmt->bindValue(":id_person", $id);
        $stmt->execute();
        $tplVars['contacts'] = $stmt->fetchAll();
        $tplVars['pid'] = $id;
        $types = $this->db->query("SELECT * FROM contact_type");
        $tplVars['types'] = $types->fetchAll();
    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }
    return $this->view->render($response, 'contacts.latte', $tplVars);

})->setName('contacts');

$app->post('/contacts', function (Request $request, Response $response){
    $form = $request->getParsedBody();

    try {
        $location = $this->db->prepare("INSERT INTO contact (id_contact, id_person, id_contact_type, contact) VALUES (nextval('contact_id_contact_seq'), :id_person, :id_contact_type, :contact)");
        $location->bindValue(':id_person', $form['id_person']);
        $location->bindValue(':id_contact_type', $form['id_contact_type']);
        $location->bindValue(':contact', $form['contact']);
        $location->execute();

    } catch (PDOException $e) {
        $this->logger->error($e->getMessage());
        die($e);
    }

    return $response->withHeader('Location', $this->router->pathFor('index'));
});

$app->get('/delete-contact', function (Request $request, Response $response, $args) {
    $id = $request->getQueryParam("id");
    try {
        $stmt = $this->db->prepare("DELETE FROM contact WHERE id_contact = :id_contact");
        $stmt->bindValue(":id_contact", $id);
        $stmt->execute();

    } catch (Exception $e) {
        $this->logger->error($e->getMessage());
        die($e->getMessage());

    }
    return $response->withHeader('Location', $this->router->pathFor('index'));

})->setName('deleteContact');


















